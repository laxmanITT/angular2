System.register(['angular2/core', './form.service', './input.directirect'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, form_service_1, input_directirect_1;
    var FormComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (form_service_1_1) {
                form_service_1 = form_service_1_1;
            },
            function (input_directirect_1_1) {
                input_directirect_1 = input_directirect_1_1;
            }],
        execute: function() {
            FormComponent = (function () {
                function FormComponent(nameService) {
                    this.title = "it is my first page components";
                    this.names = nameService.getName();
                }
                FormComponent = __decorate([
                    core_1.Component({
                        selector: 'myform',
                        template: "{{title}} \n\n<div class=\"myclass\">laxman</div>\n<input type=\"text\" auto/>\n<table border=\"1\" collaspe>\n       <caption> <h2>Employ List </h2> </caption>\n               <tr  *ngFor=\"#name of names\"   >\n                       <td>{{name.SR}}<td>\n                      <td>{{name.EmployName}}<td>\n                       <td>{{name.ReportingManager}}<td>\n                       <td>{{name.Technology}}<td>\n                 </tr>\n</table>\n",
                        styles: ['.myclass{color:red }'],
                        providers: [form_service_1.nameService], directives: [input_directirect_1.InputEventDirective]
                    }), 
                    __metadata('design:paramtypes', [(typeof (_a = typeof form_service_1.nameService !== 'undefined' && form_service_1.nameService) === 'function' && _a) || Object])
                ], FormComponent);
                return FormComponent;
                var _a;
            }());
            exports_1("FormComponent", FormComponent);
        }
    }
});
//# sourceMappingURL=form.component.js.map