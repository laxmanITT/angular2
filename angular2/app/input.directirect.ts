import {Directive,ElementRef,Renderer}  from 'angular2/core';


@Directive(
    {
        selector: '[auto]',
        host: {

            '(focus)' :'onFocus()',
            '(blur)' : 'onBlur()'

        }})

export class InputEventDirective
{


    constructor(  private el:ElementRef, private renderer:Renderer) {
        
        
    }
    onFocus(){
        
        this.renderer.setElementStyle(this.el,'height','400px')
       
       
        
        
    }
onBlur(){ this.renderer.setElementStyle(this.el,'width','200px')}

}