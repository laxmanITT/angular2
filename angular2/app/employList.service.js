System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var EmployListService;
    return {
        setters:[],
        execute: function() {
            EmployListService = (function () {
                function EmployListService() {
                    this.employList = [];
                }
                EmployListService.prototype.getEmployList = function () {
                    return this.employList;
                };
                EmployListService.prototype.SetEmployList = function (SR, EmployName, ReportingManager, Technology) {
                    this.employList.push({
                        SR: SR,
                        EmployName: EmployName,
                        ReportingManager: ReportingManager,
                        Technology: Technology
                    });
                };
                return EmployListService;
            }());
            exports_1("EmployListService", EmployListService);
        }
    }
});
//# sourceMappingURL=EmployList.service.js.map