System.register(['angular2/core', './EmployList.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, EmployList_service_1;
    var Table1Component;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (EmployList_service_1_1) {
                EmployList_service_1 = EmployList_service_1_1;
            }],
        execute: function() {
            Table1Component = (function () {
                function Table1Component(employ) {
                    this.size = 10;
                    this.employ = employ;
                    this.names = employ.getEmployList();
                }
                Table1Component.prototype.onClick = function (EmployName, ReportingManage, Technology) {
                    this.employ.SetEmployList((this.names.length + 1), EmployName, ReportingManage, Technology);
                    this.names = this.employ.getEmployList();
                };
                Table1Component = __decorate([
                    core_1.Component({
                        selector: 'table1',
                        template: "<div class=\"main\">\n                <input type=\"number\" class=\"input\"  [(ngModel)]=\"size\" placeholder=\"Your Content font size\">\n             <label><h2><i>New Employ entry</i></h2></label>\n          <form>\n               <input type=\"text\" class=\"input\" #employName placeholder=\"employ Name\">\n               <input type=\"text\"  class=\"input\"  #ReportingManage  placeholder=\"Reporting Manage\">\n               <input type=\"text\" #Technology class=\"input\" placeholder=\"Technology\">\n               <button (click)=\"onClick(employName.value,ReportingManage.value,Technology.value)\" class=\"submit\" >ADD  </button>\n                 \n                 \n                </form> </div>\n\n\n\n          <table  [ngStyle]=\"{'font-size':size}\">\n                  <caption> <h2>Employ List </h2> </caption>\n                   <tr class=\"head\" >\n                        <th>SR. NO</th>\n                        <th></th>  \n                        <th>employ Name</th>\n                             <th></th>\n                        <th>Reporting Manage</th>\n                       <th></th>\n                        <th>Technology</th>\n                    </tr>\n                      <tr *ngFor=\"#name of names\">\n\n                         <td>{{name.SR}}<td>\n                         <td>{{name.EmployName}}<td>\n                        <td>{{name.ReportingManager}}<td>\n                         <td>{{name.Technology}}<td>\n                      </tr>\n             </table>",
                        providers: [EmployList_service_1.EmployListService]
                    }), 
                    __metadata('design:paramtypes', [EmployList_service_1.EmployListService])
                ], Table1Component);
                return Table1Component;
            }());
            exports_1("Table1Component", Table1Component);
        }
    }
});
//# sourceMappingURL=table.component.js.map