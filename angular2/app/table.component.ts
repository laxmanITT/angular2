import {Component} from 'angular2/core';
import {EmployListService} from './EmployList.service';



@Component({
    selector: 'table1',
    template: `<div class="main">
                <input type="number" class="input"  [(ngModel)]="size" placeholder="Your Content font size">
             <label><h2><i>New Employ entry</i></h2></label>
          <form>
               <input type="text" class="input" #employName placeholder="employ Name">
               <input type="text"  class="input"  #ReportingManage  placeholder="Reporting Manage">
               <input type="text" #Technology class="input" placeholder="Technology">
               <button (click)="onClick(employName.value,ReportingManage.value,Technology.value)" class="submit" >ADD  </button>
                 
                 
                </form> </div>



          <table  [ngStyle]="{'font-size':size}">
                  <caption> <h2>Employ List </h2> </caption>
                   <tr class="head" >
                        <th>SR. NO</th>
                        <th></th>  
                        <th>employ Name</th>
                             <th></th>
                        <th>Reporting Manage</th>
                       <th></th>
                        <th>Technology</th>
                    </tr>
                      <tr *ngFor="#name of names">

                         <td>{{name.SR}}<td>
                         <td>{{name.EmployName}}<td>
                        <td>{{name.ReportingManager}}<td>
                         <td>{{name.Technology}}<td>
                      </tr>
             </table>`,
   
   providers:[EmployListService]
})


export class Table1Component  {
   
    size:number=10;
    names:any[];
    employ:EmployListService;
constructor(employ:EmployListService)
{
    this.employ = employ;
    this.names=employ.getEmployList();
}

onClick(EmployName,ReportingManage,Technology)
{

     
    this.employ.SetEmployList((this.names.length+1),EmployName,ReportingManage,Technology);
    this.names=this.employ.getEmployList();

}


}