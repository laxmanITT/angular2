import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './COMPONENT/app.login.component'
import {RegistrationComponent} from './COMPONENT/app.registration.component'
import {DashboardModule} from './dashboard.module'
import {PageNotFound} from './COMPONENT/pagenotFound'
import { AppComponent }  from './app.component';
import {RouterModule} from '@angular/router'
import {DashboardComponent} from './COMPONENT/app.dashboard.component'
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import {RegisterNavComponent } from './COMPONENT/RegisterNav.component'

@NgModule({
    imports: [BrowserModule,FormsModule,DashboardModule,
              RouterModule.forRoot([
                  { path: '' , redirectTo :'/index',pathMatch: 'full'},
                  { path: 'index' ,component: RegisterNavComponent ,
                    children: [
                  { path: '' , component: LoginComponent, outlet: 'child1'},
                 {path: 'Login' , component: LoginComponent, outlet: 'child1' },
                  {path: 'Registration' , component: RegistrationComponent, outlet: 'child1'}
                   ]},
                   {path: '**' , component:  PageNotFound}
              ])

             ],
       providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
    declarations: [AppComponent,LoginComponent,RegistrationComponent,PageNotFound,RegisterNavComponent],
    bootstrap:    [ AppComponent ],

})
export class AppModule { }
