import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './COMPONENT/app.dashboard.component'
import {RouterModule,Routes} from '@angular/router'

const route: Routes =[
    {
       path: 'dashboard/:email' , component:  DashboardComponent
    
    }
]

@NgModule({
    imports: [RouterModule.forChild(route)],
    exports : [RouterModule]
   
})
export class DashboardRouteModule {
}
