

import { Component } from '@angular/core';
import {Router} from '@angular/router'
import {RegisterNavComponent } from './RegisterNav.component'


@Component({

    templateUrl: 'app/COMPONENT/HTML/login.componenents.html'
})


export class LoginComponent
{


    passwordError: string = '';
    emailError: string = '';
    loginMessage: string =  '';


    constructor(private route: Router)
    {

    }



    // onLoginSubmit method perform the
    onLoginSubmit(loginUser:any)
    {
        this.passwordError= "";
        this.emailError= "";
        this.loginMessage="";
        let   user=JSON.parse(localStorage.getItem(loginUser.email));


        if (user!=null)
        {
            if(user.email==loginUser.email)
            {
                if(user.password==loginUser.password)
                {
                    this.route.navigate(['/dashboard',loginUser.email]);
                }
                else
                {
                    this.passwordError="please enter correct password"   ;
                }
            }


        }
        else
        {
            this.emailError = "Your Email id is not register"  ;

        }




    }

}
