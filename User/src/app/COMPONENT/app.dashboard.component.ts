import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerListService } from './SERVICE/CustomerListService';
import { LocalStorageService } from './SERVICE/LocalStorageService';



@Component({

  templateUrl: 'app/COMPONENT/HTML/deshboard.components.html',
  styleUrls: ['app/COMPONENT/HTML/dashboard.css'],
  providers: [CustomerListService, LocalStorageService]


})


export class DashboardComponent {
  userName: string = '';
  email: string = '';
  public customerList: any[];
  customerName: string = "";
  customerEmail: string = "";
  customerContact: string = "";

  public List: any[] = [{

    customerName: 'Customer Name',
    customerEmail: 'Customer Email',
    customerContact: 'Customer Contact'

  }]




  user: any;
  constructor(private route: ActivatedRoute, private customerservice: CustomerListService, private storage: LocalStorageService) {


    this.email = route.snapshot.params['email'];
    this.user = this.storage.getLocalStorateData(this.email);
    this.userName = this.user.name
    if (this.user.customerList.length != 0) {

      for (let index in this.user.customerList) {
        customerservice.SetCustmerList(this.user.customerList[index]);

      }


      this.customerList = this.customerservice.getCustmerList();;



    }





  }





  onSubmit(customer: any) {

    this.customerservice.SetCustmerList(customer);
    this.customerList = this.customerservice.getCustmerList();
    this.user.customerList = this.customerList;
    this.storage.SetLocalStorateData(this.email, this.user)
    this.customerName = "";
    this.customerEmail = "";
    this.customerContact = "";
  }

  onDelete(customer: any) {
    console.log(customer);
    this.customerservice.DeleteCustomerInformation(customer);
    this.customerList = this.customerservice.getCustmerList();
    this.user.customerList = this.customerList;
    this.storage.SetLocalStorateData(this.email, this.user)

  }

}
