

import { Component } from '@angular/core';
import {Router} from '@angular/router'
import {RegisterNavComponent } from './RegisterNav.component'

@Component({

    templateUrl: 'app/COMPONENT/HTML/registration.components.html'
})


export class RegistrationComponent
{


    passwordError: string = '';
    emailError: string = '';
    RegisterMessage: string = '';
    loginMessage: string =  '';
    public RegisterEmailError: string = '';

    constructor(private route: Router)
    {

    }
    //onsubmit method perform ather the click on Register
    onSubmit(user:any )
    {




        this.checkUser(user);


    }

    //checkUser method check the user in database
    checkUser(user:any)
    {


        if(JSON.parse(localStorage.getItem(user.email))!= null)
        {
            console.log("your email already exist");
            this.RegisterEmailError="your email already exist";

        }
        else
        {
            this.addUser(user);  
        }  
    }


    //add User method add user in the localstorage
    addUser(user:any)
    {

        if (localStorage)
        {
            user.customerList=[];        
            localStorage.setItem(user.email,JSON.stringify(user));

            this.route.navigate(['/dashboard',user.email]);    

        }
        else
        {
            alert("Sorry, your browser does not support Web Storage");
        }    



    }
}