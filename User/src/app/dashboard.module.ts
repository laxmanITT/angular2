import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {DashboardComponent} from './COMPONENT/app.dashboard.component'
import {DashboardRouteModule} from './dashboardroute'


@NgModule({
    imports: [BrowserModule,FormsModule,DashboardRouteModule],
    declarations: [DashboardComponent],
    bootstrap:    [DashboardComponent ]
})
export class DashboardModule { }
